﻿using System.Data.Entity;
using eProducts.Domain.Models;

namespace eProducts.Domain
{
    /// <summary>
    /// Database context.
    /// </summary>
    public class eProductsContext : DbContext
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public eProductsContext()
            : base("name=eProductsDbConnectionString")
        {
        }

        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
    }
}
