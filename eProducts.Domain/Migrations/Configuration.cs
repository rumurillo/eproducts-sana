namespace eProducts.Domain.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<eProducts.Domain.eProductsContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(eProducts.Domain.eProductsContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            context.Categories.AddOrUpdate(new Models.Category { CategoryId = Guid.NewGuid(), Name = "Tech" });
            context.Products.AddOrUpdate(new Models.Product { ProductId = Guid.NewGuid(), Number = 12, Price = 1213, Title = "Product" });
        }
    }
}
