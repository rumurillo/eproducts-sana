﻿using System;
using System.Collections.Generic;

namespace eProducts.Domain.Models
{
    public class Order
    {
        public Guid OrderId { get; set; }
        public int Number { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}
