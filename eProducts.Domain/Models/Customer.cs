﻿using System;
using eProducts.Domain.Models;
using System.Collections.Generic;

namespace eProducts.Domain
{
    public class Customer
    {
        public Guid CustomerId { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
    }
}
