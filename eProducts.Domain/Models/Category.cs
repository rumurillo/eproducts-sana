﻿using System;
using System.Collections.Generic;

namespace eProducts.Domain.Models
{
    public class Category
    {
        public Guid CategoryId { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}
