﻿using System;
using System.Collections.Generic;

namespace eProducts.Domain.Models
{
    public class Product
    {
        public Guid ProductId { get; set; }
        public int Number { get; set; }
        public string Title { get; set; }
        public decimal Price { get; set; }
        public virtual ICollection<Category> Categories { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
    }
}
