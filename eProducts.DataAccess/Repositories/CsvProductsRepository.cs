﻿using System;
using System.IO;
using eProducts.Domain.Models;
using System.Collections.Generic;
using eProducts.Data.Abstractions;

namespace eProducts.DataAccess.Repositories
{
    /// <summary>
    /// Implements Products' csv data operations.
    /// </summary>
    public class CsvProductsRepository : IProductsRepository
    {
        /// <summary>
        /// Gets or sets Data Storage Type.
        /// </summary>
        public string DataStorageType { get; set; }

        /// <summary>
        /// Comma-separated value file path.
        /// </summary>
        public string CsvFilePath { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        public CsvProductsRepository(string dataStorageType, string csvFilePath)
        {
            DataStorageType = dataStorageType;
            CsvFilePath = csvFilePath;
        }

        /// <summary>
        /// Creates a product.
        /// </summary>
        /// <param name="product">Product to be created.</param>
        public Product Create(Product product)
        {
            product.ProductId = Guid.NewGuid();
            using (var writer = File.AppendText(CsvFilePath))
            {
                var record = $"{product.ProductId},{product.Title},{product.Number},{product.Price}";
                writer.WriteLine(record);
            }
            return product;
        }

        /// <summary>
        /// Gets all products.
        /// </summary>
        /// <returns>List of products.</returns>
        public IEnumerable<Product> GetAll()
        {
            var products = new List<Product>();

            using (var reader = new StreamReader(File.OpenRead(CsvFilePath)))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');
                    products.Add(new Product
                    {
                        ProductId = Guid.Parse(values[0]),
                        Title = values[1],
                        Number = int.Parse(values[2]),
                        Price = decimal.Parse(values[3])
                    });
                }
            }

            return products;
        }
    }
}
