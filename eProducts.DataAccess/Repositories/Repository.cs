﻿using System.Linq;
using System.Data.Entity;
using System.Collections.Generic;
using eProducts.Data.Abstractions;

namespace eProducts.DataAccess.Repositories
{
    /// <summary>
    /// IRepository implementation class.
    /// </summary>
    /// <typeparam name="TEntity">Generic entity.</typeparam>
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        /// <summary>
        /// Database context.
        /// </summary>
        public DbContext Context;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="context">Database context.</param>
        public Repository(DbContext context)
        {
            Context = context;
        }

        /// <summary>
        /// Adds a new "entity" to the context.
        /// </summary>
        /// <param name="entity">Entity to be added</param>
        /// <returns>Added entity.</returns>
        public TEntity Add(TEntity entity)
        {
            Context.Set<TEntity>().Add(entity);
            Context.SaveChanges();
            return entity;
        }

        /// <summary>
        /// Gets all records of type "entity".
        /// </summary>
        /// <returns>List of entity</returns>
        public IEnumerable<TEntity> Get()
        {
            return Context.Set<TEntity>().ToList();
        }
    }
}
