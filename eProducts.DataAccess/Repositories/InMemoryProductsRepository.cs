﻿using System;
using System.Web;
using System.Linq;
using eProducts.Domain.Models;
using System.Collections.Generic;
using eProducts.Data.Abstractions;

namespace eProducts.DataAccess.Repositories
{
    /// <summary>
    /// Implements Products' in-memory data operations.
    /// </summary>
    public class InMemoryProductsRepository : IProductsRepository
    {
        /// <summary>
        /// Session key.
        /// </summary>
        private const string SessionKey = "InMemoryStorage";

        /// <summary>
        /// Gets or sets Data Storage Type.
        /// </summary>
        public string DataStorageType { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="dataStorageType">Data storage type.</param>
        public InMemoryProductsRepository(string dataStorageType)
        {
            DataStorageType = dataStorageType;
        }

        /// <summary>
        /// Creates a product.
        /// </summary>
        /// <param name="newProduct">Product to be created.</param>
        public Product Create(Product newProduct)
        {
            var products = GetAll().ToList();
            newProduct.ProductId = Guid.NewGuid();
            products.Add(newProduct);

            HttpContext.Current.Session[SessionKey] = products;
            return newProduct;
        }

        /// <summary>
        /// Gets all products.
        /// </summary>
        /// <returns>List of products.</returns>
        public IEnumerable<Product> GetAll()
        {
            var products = (List<Product>)HttpContext.Current.Session[SessionKey];

            return products;
        }
    }
}
