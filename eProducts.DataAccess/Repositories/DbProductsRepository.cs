﻿using System;
using eProducts.Domain;
using eProducts.Domain.Models;
using System.Collections.Generic;
using eProducts.Data.Abstractions;

namespace eProducts.DataAccess.Repositories
{
    /// <summary>
    /// Implements Products' database data operations.
    /// </summary>
    public class DbProductsRepository : Repository<Product>, IProductsRepository
    {
        /// <summary>
        /// Gets or sets Data Storage Type.
        /// </summary>
        public string DataStorageType { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="context">Database context.</param>
        /// <param name="dataStorageType">Data storage type.</param>
        public DbProductsRepository(eProductsContext context, string dataStorageType)
           : base(context)
        {
            Context = context;
            DataStorageType = dataStorageType;
        }

        /// <summary>
        /// Gets all products.
        /// </summary>
        /// <returns>List of products.</returns>
        public IEnumerable<Product> GetAll()
        {
            return Get();
        }

        /// <summary>
        /// Creates a product.
        /// </summary>
        /// <param name="product">Product to be created.</param>
        public Product Create(Product product)
        {
            product.ProductId = Guid.NewGuid();
            Add(product);

            return product;
        }
    }
}
