﻿using System.Collections.Generic;

namespace eProducts.Data.Abstractions
{
    /// <summary>
    /// Generic Repository interface.
    /// </summary>
    /// <typeparam name="TEntity">Generic entity.</typeparam>
    public interface IRepository<TEntity> where TEntity : class
    {
        /// <summary>
        /// Gets all records of type "entity".
        /// </summary>
        /// <returns>List of entity</returns>
        IEnumerable<TEntity> Get();

        /// <summary>
        /// Adds a new "entity" to the context.
        /// </summary>
        /// <param name="entity">Entity to be added</param>
        /// <returns>Added entity.</returns>
        TEntity Add(TEntity entity);
    }
}
