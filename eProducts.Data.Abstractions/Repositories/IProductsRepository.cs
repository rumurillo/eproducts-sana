﻿using eProducts.Domain.Models;
using System.Collections.Generic;

namespace eProducts.Data.Abstractions
{
    /// <summary>
    /// Product repository interface.
    /// </summary>
    public interface IProductsRepository
    {
        /// <summary>
        /// Gets or sets Data Storage type.
        /// </summary>
        string DataStorageType { get; set; }

        /// <summary>
        /// Gets all products.
        /// </summary>
        /// <returns>List of products.</returns>
        IEnumerable<Product> GetAll();

        /// <summary>
        /// Creates a product.
        /// </summary>
        /// <param name="product">Product to be created.</param>
        Product Create(Product product);
    }
}
