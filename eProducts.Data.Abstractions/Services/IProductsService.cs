﻿using eProducts.Domain.Models;
using System.Collections.Generic;

namespace eProducts.Data.Abstractions.Services
{
    /// <summary>
    /// Product Service interface.
    /// </summary>
    public interface IProductsService
    {
        /// <summary>
        /// Gets or sets Data Storage type.
        string DataStorageType { get; set; }

        /// <summary>
        /// Gets all products.
        /// </summary>
        /// <returns>List of products.</returns>
        IEnumerable<Product> GetAll();

        /// <summary>
        /// Adds a product.
        /// </summary>
        /// <param name="product">Product to be stored.</param>
        Product Create(Product product);
    }
}
