﻿using System.Linq;
using eProducts.Domain.Models;
using System.Collections.Generic;
using eProducts.Data.Abstractions;
using eProducts.Data.Abstractions.Services;

namespace eProducts.Service
{
    /// <summary>
    /// In-memory products service class implementation.
    /// </summary>
    public class InMemoryProductsService : IProductsService
    {
        /// <summary>
        /// Gets or sets Data Storage Type.
        /// </summary>
        public string DataStorageType { get; set; }

        /// <summary>
        /// Products repository.
        /// </summary>
        private readonly IEnumerable<IProductsRepository> _productsRepositories;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="productsRepositories">Injection of product repository.</param>
        /// <param name="dataStorageType">Injection of data storage type.</param>
        public InMemoryProductsService(IEnumerable<IProductsRepository> productsRepositories, string dataStorageType)
        {
            _productsRepositories = productsRepositories;
            DataStorageType = dataStorageType;
        }

        /// <summary>
        /// Creates a product.
        /// </summary>
        /// <param name="product">Product to be created.</param>
        public Product Create(Product product)
        {
            // Gets the injected implementation based on dataStorageType.
            var repository = _productsRepositories.First(x => x.DataStorageType == DataStorageType);

            var newProduct = repository.Create(product);
            return newProduct;
        }

        /// <summary>
        /// Gets all products.
        /// </summary>
        /// <returns>List of products.</returns>
        public IEnumerable<Product> GetAll()
        {
            // Gets the injected implementation based on dataStorageType.
            var repository = _productsRepositories.First(x => x.DataStorageType == DataStorageType);

            return repository.GetAll();
        }
    }
}
