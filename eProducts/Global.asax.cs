﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using eProducts.App_Start;
using eProducts.Domain.Models;
using System.Web.Optimization;
using System.Collections.Generic;

namespace eProducts
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutofacConfig.Register();
        }

        protected void Session_Start()
        {
            var products = new List<Product>
            {
                new Product{ ProductId = Guid.NewGuid(), Number = 1, Price = 1234, Title = "Product" }
            };

            HttpContext.Current.Session["InMemoryStorage"] = products;
        }
    }
}
