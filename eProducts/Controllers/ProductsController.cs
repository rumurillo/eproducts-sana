﻿using System;
using System.Linq;
using System.Web.Mvc;
using eProducts.Models;
using eProducts.Domain.Models;
using System.Collections.Generic;
using eProducts.Data.Abstractions.Services;

namespace eProducts.Controllers
{
    /// <summary>
    /// Products controller.
    /// </summary>
    public class ProductsController : Controller
    {
        /// <summary>
        /// Lazy injection of a list of IProductsService interface.
        /// </summary>
        private readonly Lazy<IEnumerable<IProductsService>> _productServices;

        /// <summary>
        /// Default data storage.
        /// </summary>
        public const string DataStorage = "Db";

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="productServices">List of IProductsService implementations.</param>
        public ProductsController(Lazy<IEnumerable<IProductsService>> productServices)
        {
            _productServices = productServices;
        }

        // GET: Products
        public ActionResult Index(string dataStorageType = DataStorage)
        {
            // Gets the injected implementation based on dataStorageType.
            var service = _productServices.Value.First(x => x.DataStorageType == dataStorageType);

            var products = service.GetAll();

            var productViewModel = new ProductViewModel { Products = products, DataStorageType = dataStorageType };
            return View(productViewModel);
        }

        // GET: Create
        public ActionResult Create(string dataStorageType)
        {
            var newProductViewModel = new NewProductViewModel { Product = new Product(), DataStorageType = dataStorageType };
            return View(newProductViewModel);
        }

        // POST: Create
        [HttpPost]
        public ActionResult Create(NewProductViewModel newProduct)
        {
            // Gets the injected implementation based on dataStorageType.
            var service = _productServices.Value.First(x => x.DataStorageType == newProduct.DataStorageType);

            service.Create(newProduct.Product);

            return RedirectToAction("Index", new { newProduct.DataStorageType });
        }
    }
}