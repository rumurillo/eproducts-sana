﻿using eProducts.Domain.Models;
using System.Collections.Generic;

namespace eProducts.Models
{
    public class ProductViewModel
    {
        /// <summary>
        /// Gets or sets data storage type.
        /// </summary>
        public string DataStorageType { get; set; }

        /// <summary>
        /// Gets or sets list of products.
        /// </summary>
        public IEnumerable<Product> Products { get; set; }
    }
}