﻿using eProducts.Domain.Models;

namespace eProducts.Models
{
    public class NewProductViewModel
    {
        public string DataStorageType { get; set; }
        public Product Product { get; set; }
    }
}