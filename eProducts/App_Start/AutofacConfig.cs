﻿using Autofac;
using System.Web;
using System.Web.Mvc;
using eProducts.Domain;
using eProducts.Service;
using eProducts.Domain.Models;
using Autofac.Integration.Mvc;
using eProducts.Data.Abstractions;
using eProducts.DataAccess.Repositories;
using eProducts.Data.Abstractions.Services;

namespace eProducts.App_Start
{
    /// <summary>
    /// Autofac Dependency Injection configuration class.
    /// </summary>
    public class AutofacConfig
    {
        /// <summary>
        /// Storage parameter name.
        /// </summary>
        private const string StorageParameterName = "dataStorageType";

        /// <summary>
        /// Comma-separated value parameter name.
        /// </summary>
        private const string CsvPathParameterName = "csvFilePath";

        /// <summary>
        /// Comma-separated value file path.
        /// </summary>
        private readonly static string CsvFilePath = HttpContext.Current.Server.MapPath("~/Content/DataStorage/CsvProducts.csv");

        /// <summary>
        /// Registers dependencies to Autofac's dependency injection resolver.
        /// </summary>
        public static void Register()
        {
            var containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterControllers(typeof(MvcApplication).Assembly);

            // Register repositories
            containerBuilder.RegisterType<Repository<Product>>()
                .As<IRepository<Product>>()
                .InstancePerRequest();
            containerBuilder.RegisterType<DbProductsRepository>()
                .As<IProductsRepository>()
                .WithParameter("context", new eProductsContext())
                .InstancePerRequest()
                .WithParameter(new NamedParameter(StorageParameterName, "Db"));
            containerBuilder.RegisterType<CsvProductsRepository>()
                .As<IProductsRepository>()
                .InstancePerRequest()
                .WithParameter(new NamedParameter(StorageParameterName, "Csv"))
                .WithParameter(new NamedParameter(CsvPathParameterName, CsvFilePath));
            containerBuilder.RegisterType<InMemoryProductsRepository>()
                .As<IProductsRepository>()
                .InstancePerRequest()
                .WithParameter(new NamedParameter(StorageParameterName, "InMemory"));

            // Register services
            containerBuilder.RegisterType<DbProductsService>()
                .As<IProductsService>()
                .InstancePerRequest().WithParameter(new NamedParameter(StorageParameterName, "Db"));
            containerBuilder.RegisterType<CsvProductsService>()
               .As<IProductsService>()
               .InstancePerRequest().WithParameter(new NamedParameter(StorageParameterName, "Csv"));
            containerBuilder.RegisterType<InMemoryProductsService>()
                .As<IProductsService>()
                .InstancePerRequest().WithParameter(new NamedParameter(StorageParameterName, "InMemory"));

            var container = containerBuilder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}